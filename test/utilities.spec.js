const util = require('../src/utilities');
const { expect } = require('chai');
describe('test/utilities.spec.js', () => {
	describe('initConfig', () => {
		it('defaults appName if not set', () => {
			let test = {};
			util.initConfig(test);
			expect(test.appName).to.equal('bubblenode');
		})
		it('does not override appName if set', () => {
			let test = {
				appName : 'myApp'
			}
			util.initConfig(test);
			expect(test.appName).to.equal('myApp');
		})
		it('defaults port if not set', () => {
			let test = {};
			util.initConfig(test);
			expect(test.port).to.equal(8080);
		})
		it('does not override port if set', () => {
			let test = {
				port : 12
			}
			util.initConfig(test);
			expect(test.port).to.equal(12);
		})
		it('defaults log location to default appName.log if not set', () => {
			let test = {};
			util.initConfig(test);
			expect(test.logLocation).to.equal('bubblenode.log');
		})
		it('defaults log location to appName.log if not set', () => {
			let test = {
				appName : 'my-app'
			};
			util.initConfig(test);
			expect(test.logLocation).to.equal('my-app.log');
		})
		it('does not override log location if set', () => {
			let test = {
				logLocation : 'my-log.log'
			}
			util.initConfig(test);
			expect(test.logLocation).to.equal('my-log.log');
		})
	})
})