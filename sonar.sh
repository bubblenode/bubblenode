#!/bin/bash

if [ ! -d "sonar-scanner-3.0.3.778-linux/bin" ]; then
    apt-get update
    apt-get install -y unzip
    SONAR_FILENAME=sonar-scanner-cli-3.0.3.778-linux
    curl --insecure -OL https://sonarsource.bintray.com/Distribution/sonar-scanner-cli/$SONAR_FILENAME.zip
    unzip $SONAR_FILENAME.zip
fi
./sonar-scanner-3.0.3.778-linux/bin/sonar-scanner \
    -Dsonar.projectKey=$SONAR_PROJECTKEY \
    -Dsonar.sources=./src/ \
    -Dsonar.host.url=https://sonarcloud.io \
    -Dsonar.organization=$SONAR_ORGANISATION \
    -Dsonar.login=$SONAR_LOGIN