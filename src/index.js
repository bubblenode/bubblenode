const apiLoader = require('./api-loader');
const responses = require('./response');
const utilities = require('./utilities');
const apiBuilder = require('./functionals/api-builder');
const Server = require('./lib/express/server');
let bubblenode = function(config) {
	this.config = config;
	if (!config) {
		config = {};
	}
	utilities.initConfig(config);
	this.server = new Server(config);
	this.log = require('bubble-log');
	this.log.create(config.appName, config.logLocation);
	this.server.log = this.log;
	let self = this;
	this.server.addStage( function(request, response) {
		self.addLogger.apply(self, [request, response]);
	})
	let context = this.server.createContext('/api');
	apiLoader.rest(context);
	if (config.apiPath) {
		this.log.info('BUBBLENODE', `Loading from ${config.apiPath}`);
		apiLoader.rest(context, config.apiPath);
		this.server.addStatic('/', config.apiPath + '/static');
	}
	this.server.addEndpoint('/js/api.js', (req, res) => {
		res.pureResult(apiBuilder.getApi()).complete();
	});
	this.server.addStatic('/', __dirname + '/static');
	this.server.addEndpoint('/test', (req, res) => {
		console.log(res);
		res.result('test').complete();
	})
	this.server.addStage( function(request, response) {
		self.endRequest.apply(self, [request, response]);
	})
	this.server.run();
}

bubblenode.prototype.addLogger = function(req, res) {
	req.log = this.log.instance({ path: req.path });
}

bubblenode.prototype.endRequest = function(req, res) {
	console.log(res)
  if (!res.completed) {
    res.code('404').complete();
		req.log.info("REQUEST", "NOT FOUND", { status: "Complete" } );
  }
}

module.exports = { 
	BubbleNode : bubblenode,
	RestBuilder : require('./rest-builder')
};