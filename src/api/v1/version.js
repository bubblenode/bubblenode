const rest = require('../../rest-builder.js');
let version = "0.0.1";
module.exports.endpoints = [
	rest.get('getVersion', '/', (params, response) => {
		response.result(version).complete();
	}),
	rest.get('getVersionByPart', '/{part}', (params, response) => {
		let spl = version.split('.');
		if (params.part === "major") {
			response.result(spl[0]).complete();
		}else if (params.part === "minor") {
			response.result(spl[1]).complete();
		}else if (params.part === "patch") {
			response.result(spl[2]).complete();
		}
		response.error("Invalid version part").complete();
	}),
	rest.get('repeat', '/b/{part1}-{part2}', (params, response) => {
		response.result(params.part1 + "-" + params.part2).complete();
	})
]