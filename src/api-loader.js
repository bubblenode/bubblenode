let fs = require("fs");
let path = require("path");
let utils = require("./utilities");
let log = require('bubble-log');
const apiBuilder = require('./functionals/api-builder')

let loadFile = function(folder, file, router, context, parent) {
	let mod = file.replace('.js', '');
	let fileRouter = router.addContext('/' + mod);
	let api = require(folder + '/' + mod);
	let child = parent.newChild(mod);
	if ( api.hasOwnProperty('endpoints') ) {
		log.info('API_VERSIONS', 'Endpoint Found');
		api.endpoints.forEach((ep) => {
			log.info('API_VERSIONS', "Adding endpoint", { version: folder, context: mod, 
				endpoint: ep.path, endPointType : ep.type} );
			let newItem = child.addFunction(ep.name, context + "/" + mod, ep.path, ep.type)
			fileRouter[ep.type](ep, newItem.expressFormat);
		})
	}
}

let loadFolder = function(fullPath, folder, app, context, parent) {
	let router = app.addContext('/' + folder);
	let normalizedPath = path.join(fullPath, folder);
	let child = parent.newChild(folder);
	let details = utils.getFolderDetails(normalizedPath);

	details.files.forEach( (file)=> {
		loadFile(normalizedPath, file, router, context, child);
	});
	details.folders.forEach( (folder) => {
		loadFolder(normalizedPath + "/", folder, router, context + "/" + folder, child);
	})
}

module.exports.rest = function( app, path = __dirname ) {
	utils.getFolders(path + "/api").forEach( (v) => loadFolder(path + "/api", v, app, "/api/" + v, apiBuilder.api) );
}
