const fs = require('fs');
const path = require('path');
const log = require('bubble-log');

module.exports.getFiles = function(srcpath) {
  let files = fs.readdirSync(srcpath).filter( function(file) {
    let lstat = fs.lstatSync(path.join(srcpath, file));
    return !lstat.isDirectory();
  });
  log.info('UTILITIES', 'Loaded files', { path: srcpath, result: files});
  return files;
}

module.exports.getFolders = function(srcpath) {
  let folders = fs.readdirSync(srcpath).filter( function(file) {
    let lstat = fs.lstatSync(path.join(srcpath, file));
    return lstat.isDirectory();
  });
  log.info('UTILITIES', 'Loaded folders', { path: srcpath, results: folders });
  return folders;
};

module.exports.getFolderDetails = function(srcpath) {
  let items = fs.readdirSync(srcpath);
  let result = {
    files : [],
    folders : []
  }
  items.forEach((i) => {
    let lstat = fs.lstatSync(path.join(srcpath, i));
    if (lstat.isDirectory()) {
      result.folders.push(i);
    } else {
      result.files.push(i);
    }
  })
  return result;
}

module.exports.initConfig = function(config) {
	if (!config.appName)
		config.appName = 'bubblenode';
	if (!config.port)
		config.port = 8080;
	if (!config.logLocation)
		config.logLocation = `${config.appName}.log`;
}