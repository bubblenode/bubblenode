module.exports.get = function( name, path, fn ) {
  return { name: name, path: path, type: 'get', fn : fn };
}