const BaseContext = require('../base/context');
const express = require('express');
function Context() {
	this.name = 'Context';
}

Context.prototype = new BaseContext();

Context.prototype.get = function(endpoint, format) {
	this.router.get(format, (req, res) => {
		endpoint.fn(req.params, res.response);
	})
}

Context.prototype.addContext = function(path) {
	let context = new Context();
	context.endpoint = path;
	context.router = express.Router();
	this.router.use(path, context.router);
	return context;
}

module.exports = Context;