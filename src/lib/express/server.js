const Server = require('../base/server');
const express = require('express');
const pretty = require('express-prettify');
const Responses = require('../../response');
const Context = require('./context');

function ExpressServer(config) {
	this.config = config;
	this.app = express();
	this.app.use(pretty({ query: 'pretty' }));
	this.app.use((req, res, next) => {
		res.response = Responses.builder(res);
		next();
	})
}

ExpressServer.prototype = new Server();

ExpressServer.prototype.translate = function(fn) {
	return (req, res, next) => {
		fn(req.params, res.response);
		next();
	}
}

ExpressServer.prototype.addStage = function(fn) {
	this.app.use(this.translate(fn));
}

ExpressServer.prototype.addEndpoint = function(endpoint, fn) {
	this.app.use(endpoint, this.translate(fn));
}

ExpressServer.prototype.addStatic = function(endpoint, path) {
	this.app.use(endpoint, express.static(path));
}

ExpressServer.prototype.createContext = function(endpoint) {
	let context = new Context();
	context.endpoint = endpoint;
	context.router = express.Router();
	this.app.use(endpoint, context.router);
	return context;
}

ExpressServer.prototype.run = function() {
	let self = this;
	this.app.listen(this.config.port, function() {
		self.log.info('BUBBLENODE', `${self.config.appName} listening on ${self.config.port}`);
	});
}

module.exports = ExpressServer;