
var messageBuilder = {
  builder : function(res) {
    var ret = { 
      response : res,
      completed : false,
      obj : {
        code : "SUCCESS"
      }
    };
    ret.error = function(msg) {
			if (!ret.obj.errors)
				ret.obj.errors = [];
      ret.obj.errors.push(msg);
      ret.obj.code = "ERROR";
      return ret;
    };
    ret.code = function(code) {
      ret.obj.code = code.toUpperCase()
      return ret;
    };
    ret.result = function(data) {
      ret.obj.data = data;
      return ret;
    };
    ret.complete = function() {
      if (!ret.completed) {
        ret.completed = true;
        ret.response.send(ret.obj);
      }
    }
    ret.pureResult = function (result) {
      ret.obj = result;
      return ret;
    }
    return ret;
  }
};

module.exports = messageBuilder;