let endpointData = function(name) {
	this.name = name;
	this.children = [];
	this.functions = [];
}

endpointData.prototype.newChild = function(name) {
	let res = new endpointData(name);
	this.children.push(res);
	return res;
}

endpointData.prototype.addFunction = function(name, context, path, type) {
	let params = [];
	let regex = /{(.*?)}/g;
	let res;
	let path2 = path;
	while (res = regex.exec(path)) {
		params.push(res[1]);
		path2 = path2.replace(res[0], ':' + res[1]);
	}
	let result = { 
		name: name, 
		path: context + path, 
		type: type, 
		params : params,
		expressFormat : path2
	};
	this.functions.push(result)
	return result;
}

let result = {
	api : new endpointData('api')
}
result.getApi = () => {
	return result;
}

module.exports = result;